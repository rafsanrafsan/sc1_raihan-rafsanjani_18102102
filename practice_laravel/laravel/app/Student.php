<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['id','nim','name','gender','departement','address','created_at','updated_at'];
}
