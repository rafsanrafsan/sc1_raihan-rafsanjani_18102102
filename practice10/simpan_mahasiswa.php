<?php
include "koneksi.php";
include "create_message.php";

$target_dir = "C:/xampp/htdocs/ReyPemweb/practice10/uploads/";
$target_file = $target_dir . basename($_FILES["foto"]["name"]);
$error = false;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["foto"]["tmp_name"]);
    if ($check !== false) {
        create_message("File is an image-" . $check["mime"] . ".", "danger", "warning");
        $error = false;
    } else {
        create_message("File is not an image.", "danger", "warning");
        $error = false;
    }
}

if (file_exists($target_file)) {
    create_message("Sorry,file already exists.","danger", "warning");
    $error = true;
}

if ($_FILES["foto"]["size"] > 500000) {
    create_message("Sorry,your file is too large.","danger", "warning");
    $error = true;
}

if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
    create_message("Sorry, only JPG,JPEG,PNG & GIF files are allowed.","danger", "warning");
    $error = true;
}

if ($error == true) {
    
    //echo "<script>console.log(".basename($_FILES["foto"]["name"]).")<script>"
    
    //create_message("Sorry,your file was not uploaded.","danger", "warning");
    header("location:index.php");
    exit();
} else {
    if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
    
    	$filename = basename($_FILES["foto"]["name"]);
    
        if (isset($_POST['mahasiswa_id'])) { //Kondisi Update
        
            $sql = "UPDATE mahasiswa SET 
            nama_lengkap ='" . $_POST['nama_lengkap'] . "',
            alamat='" . $_POST['alamat'] . "',
            kelas_id='" . $_POST['kelas_id'] . "',
            foto='" . $filename . "'
            WHERE(`mahasiswa_id`='" . $_POST['mahasiswa_id'] . "');";
        
            if ($conn->query($sql) === TRUE) {
                $conn->close();
        
                create_message("Ubah Data Berhasil", "success", "check");
                header("location:" . $_SERVER['HTTP_REFERER']);
                exit();
            } else {
                $conn->close();
                create_message("Error:" . $sql . "<br>" . $conn->error, "danger", "warning");
                header("location:" . $_SERVER['HTTP_REFERER']);
                exit();
            }
        } else { //Kondisi Insert 
            $sql = "INSERT INTO mahasiswa(nama_lengkap,kelas_id,alamat,foto) VALUES (
                '".$_POST['nama_lengkap']."', 
                ".$_POST['kelas_id'].", 
                '".$_POST['alamat']."', 
                '".$filename."'
                )";
            
            if ($conn->query($sql) === TRUE) {
                $conn->close();
                create_message("Simpan Data Berhasil", "success", "check");
                header("location:index.php");
                exit();
            } else {
                $conn->close();
                create_message("Error:" . $sql . "<br>" . $conn->error, "danger", "warning");
                header("location:index.php");
                exit();
            }
        }
        
    } else {
        create_message("Sorry,there was an error uploading your file.","danger", "warning");
        header("location:index.php");
        exit();
    }
}   