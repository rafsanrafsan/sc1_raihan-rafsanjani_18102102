<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>*{box-sizing:border-box}.row::after{content:"";clear:both;display:table}[class*="col-"]{float:left;padding:15px}html{font-family:"Lucida Sans",sans-serif}.header{background-color:#93c;color:#fff;padding:15px}.menu ul{list-style-type:none;margin:0;padding:0}.menu li{padding:8px;margin-bottom:7px;background-color:#33b5e5;color:#fff;box-shadow:0 1px 3px rgba(0,0,0,.12) , 0 1px 2px rgba(0,0,0,.24)}.menu li:hover{background-color:#09c}.aside{background-color:#33b5e5;padding:15px;color:#fff;text-align:center;font-size:14px;box-shadow:0 1px 3px rgba(0,0,0,.12) , 0 1px 2px rgba(0,0,0,.24)}.footer{background-color:#09c;color:#fff;text-align:center;font-size:12px;padding:15px}[class*="col-"]{width:100%}@media only screen and (min-width:600px){.col-s-3{width:25%}.col-s-9{width:75%}.col-s-12{width:100%}}@media only screen and (min-width:768px){.col-3{width:25%}.col-6{width:50%}}</style>
    </head>
    <body>
        <div class="header">
            <h1>Raihan Rafsanjani | 18102102 | S1 IF06 SC1</h1>
        </div>

        <div class="row">
            <div class="col-3 col-s-3 menu">
                <ul>
                    <a href="phpsyntax.php"><li>PHP Syntax</li></a>
                    <a href="phpcomment.php"><li>PHP Comments</li></a>
                    <a href="phpvariabel.php"><li>PHP Variables</li></a>
                    <a href="phpprint.php"><li>PHP Echo / Print</li></a>
                    <a href="phpdatatype.php"><li>PHP Data Types</li></a>
                    <a href="phpstrings.php"><li>PHP Strings</li></a>
                    <a href="phpnumbers.php"><li>PHP Numbers</li></a>
                    <a href="phpmath.php"><li>PHP Math</li></a>
                    <a href="phpconstants.php"><li>PHP Constants</li></a>
                    <a href="phpoperators.php"><li>PHP Operators</li></a>
                    <a href="phpifelse.php"><li>PHP IF Else ElseIF</li></a>
                    <a href="phpswitch.php"><li>PHP Switch</li></a>
                    <a href="phploops.php"><li>PHP Loops</li></a>
                    <a href="phpfunctions.php"><li>PHP Fucntions</li></a>
                    <a href="phparrays.php"><li>PHP Arrays</li></a>
                    <a href="phpsuperglobals.php"><li>PHP Super Globals</li></a>
                    <a href="phpregex.php"><li>PHP RegEx</li></a>
                  </ul>
            </div>

            <div class="col-6 col-s-9">
                <h1>Tugas Pertemuan 8</h1>
                <p>Karna di body isi konten nya bebas jadi kita isi konten ini aja gaes wkwkwk </p>
                <p>Pentatonic Scale </p>
                  <ol>
                    <h3>Apa itu Pentatonic Scale????</h3>
                    <p>Scale Pentatonic atau tangga nada pentatonik adalah suatu skala dalam musik dengan lima not
                       per oktaf. Skala pentatonik biasanya digunakan sebagai dasar dalam memainkan jenis musik blues.
                        Ada dua skala pentatonik yang paling sering digunakan yaitu skala pentatonik "Major dan skala
                         pentatonik "Minor".</p> <br> <br> <br>

                         <p> udah itu aja xixixi</p>
                  </ol>
            </div>

            <div class="col-3 col-s-12">
                <div class="aside">
                    <h2>Tentang Saya</h2>
                    <p>Nama saya adalah Raihan Rafsanjani NIM 18102102 Kelas S1 IF06 SC1 Hobby Bengong</p>
                </div>
            </div>
        </div>

        <div class="footer">
            <p>Resize the browser window to see how the content respond to the resizing.</p>
          </div>          
    <script data-pagespeed-no-defer>(function(){function f(b){var a=window;if(a.addEventListener)a.addEventListener("load",b,!1);else if(a.attachEvent)a.attachEvent("onload",b);else{var c=a.onload;a.onload=function(){b.call(this);c&&c.call(this)}}};window.pagespeed=window.pagespeed||{};var k=window.pagespeed;function l(b,a,c,g,h){this.h=b;this.i=a;this.l=c;this.j=g;this.b=h;this.c=[];this.a=0}l.prototype.f=function(b){for(var a=0;250>a&&this.a<this.b.length;++a,++this.a)try{document.querySelector(this.b[this.a])&&this.c.push(this.b[this.a])}catch(c){}this.a<this.b.length?window.setTimeout(this.f.bind(this),0,b):b()};
k.g=function(b,a,c,g,h){if(document.querySelector&&Function.prototype.bind){var d=new l(b,a,c,g,h);f(function(){window.setTimeout(function(){d.f(function(){for(var a="oh="+d.l+"&n="+d.j,a=a+"&cs=",b=0;b<d.c.length;++b){var c=0<b?",":"",c=c+encodeURIComponent(d.c[b]);if(131072<a.length+c.length)break;a+=c}k.criticalCssBeaconData=a;var b=d.h,c=d.i,e;if(window.XMLHttpRequest)e=new XMLHttpRequest;else if(window.ActiveXObject)try{e=new ActiveXObject("Msxml2.XMLHTTP")}catch(m){try{e=new ActiveXObject("Microsoft.XMLHTTP")}catch(n){}}e&&
(e.open("POST",b+(-1==b.indexOf("?")?"?":"&")+"url="+encodeURIComponent(c)),e.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),e.send(a))})},0)})}};k.criticalCssBeaconInit=k.g;})();
pagespeed.selectors=["*",".aside",".col-1",".col-10",".col-11",".col-12",".col-2",".col-3",".col-4",".col-5",".col-6",".col-7",".col-8",".col-9",".col-s-1",".col-s-10",".col-s-11",".col-s-12",".col-s-2",".col-s-3",".col-s-4",".col-s-5",".col-s-6",".col-s-7",".col-s-8",".col-s-9",".footer",".header",".menu li",".menu ul",".row","[class*=\"col-\"]","html"];pagespeed.criticalCssBeaconInit('/ngx_pagespeed_beacon','http://trihastuti.if06sc2.xyz/practice8/','c_DGtnzDvR','uWq4wADM_DI',pagespeed.selectors);</script><noscript class="psa_add_styles"><style>*{box-sizing:border-box}.row::after{content:"";clear:both;display:table}[class*="col-"]{float:left;padding:15px}html{font-family:"Lucida Sans",sans-serif}.header{background-color:#93c;color:#fff;padding:15px}.menu ul{list-style-type:none;margin:0;padding:0}.menu li{padding:8px;margin-bottom:7px;background-color:#33b5e5;color:#fff;box-shadow:0 1px 3px rgba(0,0,0,.12) , 0 1px 2px rgba(0,0,0,.24)}.menu li:hover{background-color:#09c}.aside{background-color:#33b5e5;padding:15px;color:#fff;text-align:center;font-size:14px;box-shadow:0 1px 3px rgba(0,0,0,.12) , 0 1px 2px rgba(0,0,0,.24)}.footer{background-color:#09c;color:#fff;text-align:center;font-size:12px;padding:15px}[class*="col-"]{width:100%}@media only screen and (min-width:600px){.col-s-1{width:8.33%}.col-s-2{width:16.66%}.col-s-3{width:25%}.col-s-4{width:33.33%}.col-s-5{width:41.66%}.col-s-6{width:50%}.col-s-7{width:58.33%}.col-s-8{width:66.66%}.col-s-9{width:75%}.col-s-10{width:83.33%}.col-s-11{width:91.66%}.col-s-12{width:100%}}@media only screen and (min-width:768px){.col-1{width:8.33%}.col-2{width:16.66%}.col-3{width:25%}.col-4{width:33.33%}.col-5{width:41.66%}.col-6{width:50%}.col-7{width:58.33%}.col-8{width:66.66%}.col-9{width:75%}.col-10{width:83.33%}.col-11{width:91.66%}.col-12{width:100%}}</style></noscript><script data-pagespeed-no-defer>(function(){function b(){var a=window,c=e;if(a.addEventListener)a.addEventListener("load",c,!1);else if(a.attachEvent)a.attachEvent("onload",c);else{var d=a.onload;a.onload=function(){c.call(this);d&&d.call(this)}}};var f=!1;function e(){if(!f){f=!0;for(var a=document.getElementsByClassName("psa_add_styles"),c=0,d;d=a[c];++c)if("NOSCRIPT"==d.nodeName){var k=document.createElement("div");k.innerHTML=d.textContent;document.body.appendChild(k)}}}function g(){var a=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||null;a?a(function(){window.setTimeout(e,0)}):b()}
var h=["pagespeed","CriticalCssLoader","Run"],l=this;h[0]in l||!l.execScript||l.execScript("var "+h[0]);for(var m;h.length&&(m=h.shift());)h.length||void 0===g?l[m]?l=l[m]:l=l[m]={}:l[m]=g;})();
pagespeed.CriticalCssLoader.Run();</script></body>
</html>